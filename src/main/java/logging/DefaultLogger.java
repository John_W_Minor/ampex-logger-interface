package logging;

public class DefaultLogger implements IAmpexLogger
{
    @Override
    public void trace(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Trace: " + _message);
    }

    @Override
    public void info(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Info: " + _message);
    }

    @Override
    public void debug(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Debug: " + _message);
    }

    @Override
    public void warn(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Warn: " + _message);
    }

    @Override
    public void warn(String _message, Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Warn: " + _message + " Throwable: " + _throwable.toString());
    }

    @Override
    public void warn(Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Warn Throwable: " + _throwable.toString());
    }

    @Override
    public void error(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Error: " + _message);
    }

    @Override
    public void error(String _message, Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Error: " + _message + " Throwable: " + _throwable.toString());
    }

    @Override
    public void error(Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Error Throwable: " + _throwable.toString());
    }

    @Override
    public void fatal(String _message)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Fatal: " + _message);
    }

    @Override
    public void fatal(String _message, Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Fatal: " + _message + " Throwable: " + _throwable.toString());
    }

    @Override
    public void fatal(Throwable _throwable)
    {
        System.out.println("["+ System.currentTimeMillis() +"] Fatal Throwable: " + _throwable.toString());
    }
}
