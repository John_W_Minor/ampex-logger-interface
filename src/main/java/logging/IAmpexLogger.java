package logging;

public interface IAmpexLogger
{
    void trace(String _message);
    void info(String _message);
    void debug(String _message);
    void warn(String _message);
    void warn(String _message, Throwable _throwable);
    void warn(Throwable _throwable);
    void error(String _message);
    void error(String _message, Throwable _throwable);
    void error(Throwable _throwable);
    void fatal(String _message);
    void fatal(String _message, Throwable _throwable);
    void fatal(Throwable _throwable);
}